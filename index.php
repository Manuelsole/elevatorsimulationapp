<?php

require_once 'includes/autoload.php';

use src\Services\LoadData;
use src\Services\Simulation;

$ServiceLoadData = new LoadData();
$elevators = $ServiceLoadData->fromJson(dirname(__FILE__) . '/config/elevators.json');
$periods = $ServiceLoadData->fromJson(dirname(__FILE__) . '/config/periods.json');
$serviceSimulation = new Simulation(
    $elevators,
    $periods,
    new \datetime("2021-05-02 09:00:00"),
    new \datetime("2021-05-02 20:00:00"),
);
$serviceSimulation->run();