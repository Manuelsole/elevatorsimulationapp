<?php


use PHPUnit\Framework\TestCase;
use src\Services\LoadData;
use src\Services\Simulation;

class SimulationTest extends TestCase
{

    public function testInitDateGreaterThanEndDate()
    {
        $ServiceLoadData = new LoadData();
        $elevators = $ServiceLoadData->fromJson(dirname(__FILE__) . '/../../config/elevators.json');
        $periods = $ServiceLoadData->fromJson(dirname(__FILE__) . '/../../config/periods.json');
        $initDate = new \datetime("2021-05-02 20:00:00");
        $endDate = new \datetime("2021-05-02 09:00:00");
        $this->expectException(\Exception::class);
        try {
            new Simulation($elevators, $periods, $initDate, $endDate);
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), "Simulation initial date can't be greater than end date");
            throw $e;
        }

    }

}
