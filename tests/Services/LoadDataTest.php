<?php


use PHPUnit\Framework\TestCase;
use src\Services\LoadData;

class LoadDataTest extends TestCase
{

    public function testErrorPath(){
        $path = '';
        $serviceLoadData = new LoadData();
        $this->expectException(Exception::class);
        try {
            $serviceLoadData->fromJson($path);
        } catch (Exception $e) {
            $this->assertEquals($e->getMessage(), "json file with elevators data doesn't exist");
            throw $e;
        }
    }

    public function testExatlyNumberOfElevatorsThatInSourceFile(){
        $ServiceLoadData = new LoadData();
        $elevators = $ServiceLoadData->fromJson(dirname(__FILE__) . '/../../config/elevators.json');
        $this->assertEquals(3, count($elevators));
    }
}
