<?php

namespace src\Services;

use src\Entity\Elevator;
use src\Entity\Period;

class Simulation
{
    private array $elevators;

    private array $periods;

    private \datetime $initDate;

    private \datetime $endDate;

    private \datetime $actualExecutionDate;

    /**
     * Simulation constructor.
     * @param array $elevators
     * @param array $periods
     * @param \datetime $initDate
     * @param \datetime $endDate
     */
    public function __construct(array $elevators, array $periods, \datetime $initDate, \datetime $endDate)
    {
        if($initDate->getTimestamp() > $endDate->getTimestamp()){
            throw new \Exception("Simulation initial date can't be greater than end date");
        }
        $this->elevators = $elevators;
        $this->periods = $periods;
        $this->initDate = $initDate;
        $this->endDate = $endDate;
        $this->actualExecutionDate = clone $initDate;
    }

    public function run(): void
    {
        while ($this->actualExecutionDate->getTimestamp() <= $this->endDate->getTimestamp()) {
            $this->removeDeactivatedPeriods();

            $periodsWithPendingCalls = $this->getPeriodsWithPendingCalls();
            foreach ($periodsWithPendingCalls as $period) {
                $this->executeCallsPeriod($period);
            }

            $this->reportElevatorsStatus();
            //30 secs for travel, 2 times per cycle
            $this->travelElevators();
            $this->travelElevators();
            $this->actualExecutionDate->modify('+1 minutes');
        }

        $this->finalizePosiblePendingTravelsOfElevators();
    }

    private function removeDeactivatedPeriods(): void
    {
        foreach ($this->periods as $key => $period) {
            $programatedCalls = $period->getProgramatedCalls();
            if ($this->actualExecutionDate->getTimestamp() > end($programatedCalls)) {
                unset($this->periods[$key]);
            }
        }
    }

    private function getPeriodsWithPendingCalls(): array
    {
        $arrayPeriodsWithPendingCall = [];
        foreach ($this->periods as $period) {
            if (in_array($this->actualExecutionDate->getTimestamp(), $period->getProgramatedCalls())) {
                $arrayPeriodsWithPendingCall [] = $period;
            }
        }

        return $arrayPeriodsWithPendingCall;
    }

    /**
     * @param Period $period
     */
    private function executeCallsPeriod(Period $period): void
    {
        $programatedTravels = $period->getTravelsForCall();
        $cont = 0;

        while (count($programatedTravels) > 0) {

            if ($this->checkIfElevatorsHasPendingTravels()) {
                $this->orderElevatorsByPendingTravelsAsc();
            } else {
                $this->orderElevatorsByTotalNumberTravelsAsc();
            }

            if (!$this->tryAssigneAnOptimalTravelForElevators($programatedTravels, $cont)) {
                $this->prepareFirstElevatorForAssigneTravel($this->elevators[0], $programatedTravels, $cont);
                $this->elevators[0]->addTravel($programatedTravels[$cont]['to']);
            }

            unset($programatedTravels[$cont]);
            $cont++;
        }
    }

    private function checkIfElevatorsHasPendingTravels(): bool
    {
        foreach ($this->elevators as $elevator) {
            if (!is_bool($elevator->getLastPendingTravel())) {
                return true;
            }
        }

        return false;
    }

    private function orderElevatorsByPendingTravelsAsc(): void
    {
        usort($this->elevators, function (Elevator $a, Elevator $b) {
            return $a->getNumberOfPendingTravels() <=> $b->getNumberOfPendingTravels();
        });
    }

    private function orderElevatorsByTotalNumberTravelsAsc(): void
    {
        usort($this->elevators, function (Elevator $a, Elevator $b) {
            return $a->getNumberFloorsTraveled() <=> $b->getNumberFloorsTraveled();
        });
    }

    /**
     * @param array $programatedTravels
     * @param int $cont iteration loop of while in function executeCallsPeriod
     * @return bool
     */
    private function tryAssigneAnOptimalTravelForElevators(array $programatedTravels, int $cont): bool{
        foreach ($this->elevators as $elevator) {
            if ((is_bool($elevator->getLastPendingTravel()) && $elevator->getCurrentFloor() == $programatedTravels[$cont]['from']) ||
                (!is_bool($elevator->getLastPendingTravel()) && $elevator->getLastPendingTravel() == $programatedTravels[$cont]['from'])) {
                $elevator->addTravel($programatedTravels[$cont]['to']);
                return true;
            }
        }

        return false;
    }

    /**
     * @param Elevator $elevator
     * @param array $programatedTravels
     * @param int $cont iteration loop of while in function executeCallsPeriod
     */
    private function prepareFirstElevatorForAssigneTravel(Elevator $elevator, array $programatedTravels, int $cont): void {
        if ((is_bool($elevator->getLastPendingTravel()) && $elevator->getCurrentFloor() != $programatedTravels[$cont]['from']) ||
            (!is_bool($elevator->getLastPendingTravel()) && $elevator->getLastPendingTravel() != $programatedTravels[$cont]['from'])) {
            $elevator->addTravel($programatedTravels[$cont]['from']);
        }
    }

    private function reportElevatorsStatus(): void
    {
        foreach ($this->elevators as $elevator) {
            print ($this->actualExecutionDate->format('Y-m-d H:i:s') . ' - elevator_id: ' . $elevator->getId() . ' - current_floor: ' . $elevator->getCurrentFloor() . ' - total_number_floors_traveled: ' . $elevator->getNumberFloorsTraveled() . PHP_EOL);
//            echo $actualExecutionDate->format('Y-m-d H:i:s') . ' - elevator_id: ' . $elevator->getId() . ' - current_floor: ' . $elevator->getCurrentFloor() . ' - total_number_floors_traveled: ' . $elevator->getNumberFloorsTraveled() . '<br>';
        }
    }

    private function finalizePosiblePendingTravelsOfElevators(){
        foreach ($this->elevators as $elevator) {
            while(!is_bool($elevator->getLastPendingTravel())){
                $elevator->runNextTravel();
            }
        }
    }

    /**
     * @param array $elevators
     */
    private function travelElevators(): void
    {
        foreach ($this->elevators as $elevator) {
            $elevator->runNextTravel();
        }
    }

}