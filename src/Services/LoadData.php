<?php

namespace src\Services;

use src\Entity\Elevator;
use src\Entity\Period;

class LoadData
{

    public function fromJson(string $path): array
    {
        if (!file_exists($path)) {
            throw new \Exception("json file with elevators data doesn't exist");
        }

        $jsonString = file_get_contents($path);
        $jsonData = json_decode($jsonString, true);

        switch (basename($path)) {
            case "elevators.json":
                return $this->createElevatorsArray($jsonData);
            case "periods.json":
                return $this->createPeriodsArray($jsonData);
            default:
                throw new \Exception("data type of json doesn't exist");
        }
    }

    private function createElevatorsArray($jsonData): array
    {
        $arrayData = [];
        foreach ($jsonData as $jsonUnitData) {
            $arrayData [] = Elevator::create(
                $jsonUnitData['id'],
                $jsonUnitData['currentFloor']
            );
        }

        return $arrayData;
    }

    private function createPeriodsArray($jsonData): array
    {
        $arrayData = [];
        foreach ($jsonData as $jsonUnitData) {
            $arrayData [] = Period::create(
                new \DateTime($jsonUnitData['initPeriod']),
                new \DateTime($jsonUnitData['endPeriod']),
                $jsonUnitData['frequencyCalls'],
                $jsonUnitData['fromFloor'],
                $jsonUnitData['toFloor']
            );
        }

        return $arrayData;
    }

}