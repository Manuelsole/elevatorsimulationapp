<?php

namespace src\Entity;

class Elevator
{
    private int $id, $currentFloor, $numberFloorsTraveled;
    private array $pendingTravels;

    /**
     * Elevator constructor.
     * @param int $id
     * @param int $currentFloor
     */
    public function __construct(int $id, int $currentFloor)
    {
        $this->id = $id;
        $this->currentFloor = $currentFloor;
        $this->numberFloorsTraveled = 0;
        $this->pendingTravels = [];
    }

    /**
     * @param int $id
     * @param int $currentFloor
     * @return static
     */
    public static function create(int $id, int $currentFloor): self
    {
        return new self($id, $currentFloor);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCurrentFloor(): int
    {
        return $this->currentFloor;
    }

    /**
     * @return int
     */
    public function getNumberFloorsTraveled(): int
    {
        return $this->numberFloorsTraveled;
    }

    public function getNumberOfPendingTravels(): int
    {
        return count($this->pendingTravels);
    }

    public function addTravel($floor): void
    {
        $this->pendingTravels [] = $floor;
//        if($this->id == 1){
//            echo'<pre>';var_dump($this->pendingTravels);echo'</pre>';
//        }
    }

    public function getLastPendingTravel()
    {
        return end($this->pendingTravels);
    }

    public function runNextTravel(): void
    {
        if (count($this->pendingTravels) > 0) {
            $this->currentFloor = $this->pendingTravels[0];
            $this->numberFloorsTraveled++;
            array_shift($this->pendingTravels);
        }
    }
}