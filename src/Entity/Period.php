<?php

namespace src\Entity;

class Period
{
    private \datetime $initPeriod, $endPeriod;
    private int $frequencyCalls;
    private array $fromFloor, $toFloor, $programatedCalls, $travelsForCall;

    /**
     * Period constructor.
     * @param \datetime $initPeriod
     * @param \datetime $endPeriod
     * @param int $frequencyCalls
     * @param array $fromFloor
     * @param array $toFloor
     */
    public function __construct(\datetime $initPeriod, \datetime $endPeriod, int $frequencyCalls, array $fromFloor, array $toFloor)
    {
        $this->initPeriod = $initPeriod;
        $this->endPeriod = $endPeriod;
        $this->frequencyCalls = $frequencyCalls;
        $this->fromFloor = $fromFloor;
        $this->toFloor = $toFloor;
        $this->calculateDateOfCalls();
        $this->calculateTravelsForCall();

    }

    private function calculateDateOfCalls(): void
    {
        $iterationDate = clone $this->initPeriod;
        do {
            $this->programatedCalls [] = $iterationDate->getTimestamp();
            $iterationDate->modify('+' . $this->getFrequencyCalls() . ' minutes');
        } while ($iterationDate->getTimestamp() <= $this->endPeriod->getTimestamp());
    }

    private function calculateTravelsForCall()
    {
        foreach ($this->fromFloor as $from) {
            foreach ($this->toFloor as $to) {
                $this->travelsForCall [] = ['from' => $from, 'to' => $to];
            }
        }
    }

    /**
     * @param \datetime $initPeriod
     * @param \datetime $endPeriod
     * @param int $frequencyCalls
     * @param array $fromFloor
     * @param array $toFloor
     * @return static
     */
    public static function create(\datetime $initPeriod, \datetime $endPeriod, int $frequencyCalls, array $fromFloor, array $toFloor): self
    {
        return new self($initPeriod, $endPeriod, $frequencyCalls, $fromFloor, $toFloor);
    }

    /**
     * @return int
     */
    public function getFrequencyCalls(): int
    {
        return $this->frequencyCalls;
    }

    /**
     * @return array
     */
    public function getProgramatedCalls(): array
    {
        return $this->programatedCalls;
    }

    /**
     * @return array
     */
    public function getTravelsForCall(): array
    {
        return $this->travelsForCall;
    }


}