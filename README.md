Elevator Simulation App
========================

Instructions:

1. Clone repo in you system with:
	**git clone https://Manuelsole@bitbucket.org/Manuelsole/elevatorsimulationapp.git [FOLDER]**
	
2. Go to root folder of project and execute this command:
	**vendor/php/php index.php**
	
3. Being in root folder of project, for phpunit test execute this command:
	**vendor/phpunit --bootstrap includes/autoload.php [PATH UNTIL TEST FILE]**

	example: **vendor/phpunit --bootstrap includes/autoload.php tests/Services/LoadDataTest.php**
	
4. This php app will show you the results of simulation in command line.